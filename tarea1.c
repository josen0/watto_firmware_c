/*
*  
*  tarea1.c -  Contiene la funcion tarea1,  encargada de mover los datos
*            contenidos en el buffer tmp a las estructuras que
*            contendran los datos para el posterior calculo.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
*
*/

void
tarea1 (void)
{
    static d_buffer_t* writing = 0;
    static size_t count_data = 0;
    
    tsk = 1;

   /*
   * Si -tmp- tiene por lo menos 1
   */
   
   if ((SIZE_C_BUFFER_TMP - 2) >= tmp.free)
   {

        while ((SIZE_C_BUFFER_TMP - 1) > tmp.free)
        {

        /*
        * Si el contador count_data es igual a 0, significa que se inicia con un
        * nuevo buffer a escribir datos, por tal motivo, se inicia la configuracion
        * de los punteros.
        */
        if (0 == count_data)
        {
            /*
            * Si el buffer circular write esta vacio, y tmp esta lleno
            * significa que no hay lugar donde escribir los nuevos datos
            * que provienen del ADC, por tal motivo, contituye un error fatal.
            * Pero si write esta vacio y tmp tiene cupo, no hay nada que hacer
            * solo esperar a que se desahogue el buffer read.
            */
            if (isempty_c(write) == true)
            {
               if (0 == tmp.free)
               {
                  FATAL_ERROR("wr\tmp vacio");
               }
               else
               {
               /*
               * Si el buffer write esta vacio y el buffer tmp aun tiene cupo
               * no constituye un error, ya que aun hay lugar donde almacenar
               * datos.
               */
                  return;
               }
            }

            /*
            * Se obtiene un puntero al nuevo buffer que se usara para escribir
            * datos.
            */
            writing = pop_c(&write);

            /*
            * Si el buffer de datos esta bloqueado, significa que otro proceso
            * lo esta utilizando y esto constituye un error fatal.
            */
            if (writing->busy == true)
            {
                FATAL_ERROR("writing bloqu");
            }

            /*
            * Se bloquea el nuevo buffer de datos, ya que se estara ocupando.
            */
            writing->busy = true;

        }
        
        writing->v[count_data] = tmp.v[tmp.out];
        writing->i[count_data] = tmp.i[tmp.out];
        writing->ref[count_data] = tmp.ref[tmp.out];

        tmp.v[tmp.out] = 0;
        tmp.i[tmp.out] = 0;
        tmp.ref[tmp.out] = 0;

        tmp.out = ((tmp.out == (SIZE_C_BUFFER_TMP - 1)) ? 0 : tmp.out + 1);
        tmp.free++;

        /*
        * Incrementa el indice de control.
        */
        count_data++;

        /*
        * Si cout_data = SIZE_DATA_ARRAY_BUFFER, significa que el buffer de
        * datos ha sido escrito en su totalidad.
        */
        if (SIZE_DATA_ARRAY_BUFFER == count_data)
        {
            /*
            * Revisa que el buffer lectura tenga espacio, si esta lleno es un
            * error fatal.
            */
            if (isfull_c(read))
            {
                FATAL_ERROR("read lleno");
            }

            /*
            * Libera el buffer escribir de su bloqueo, ya que se ha terminado
            * de usarlo.
            */
            writing->busy = false;

            /*
            * Reinicia el contador a 0
            */
            count_data = 0;

            /*
            * Envia escribir al buffer lectura para ser procesado.
            */
            push_c(&read, writing->ID);

            }

        }

   }


};

/*END OF FILE: tarea1.c*/
