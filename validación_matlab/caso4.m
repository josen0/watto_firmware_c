% Programa para validacion de algoritmo
% J. Nieto
% 2022
disp('>>>>>>>>>>>>>>>>>>>INICIO CASO 4');
clearvars
format longEng

syms t f;

omega = 2.*pi.*f;

v1 = sqrt(2).*100.*sin(omega.*t);
v3 = sqrt(2).*20.*sin(3.*omega.*t - degtorad(70));
v5 = sqrt(2).*25.*sin(5.*omega.*t - degtorad(-140));
v7 = sqrt(2).*10.*sin(7.*omega.*t - degtorad(-210));

i1 = sqrt(2).*60.*sin(omega.*t - degtorad(30));
i3 = sqrt(2).*15.*sin(3.*omega.*t - degtorad(165));
i5 = sqrt(2).*12.*sin(5.*omega.*t - degtorad(-285));
i7 = sqrt(2).*10.*sin(7.*omega.*t - degtorad(-310));

v = v1 + v3 + v5 + v7;

i = i1 + i3 + i5 + i7;

p = v.*i;

P = f.*(int(p, t, 0, 1./f));

disp('-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-');
disp('P = ');
disp(vpa(P));

i = subs(i, f, 60);
v = subs(v, f, 60);
P_discreta_f = subs(p, f, 60);

t_simul = 0:1./(63.*60):1./60;
disp('v = ');
vpa(subs(v, t, t_simul))
disp('i = ');
vpa(subs(i, t, t_simul))
disp('P = ');
vpa(subs(P_discreta_f, t, t_simul))

Grafica_Rectangular = zeros(49,1);
Grafica_Simpson = zeros(49,1);
Grafica_P = zeros(49,1);
Grafica_Error_Rectangular = zeros(49,1);
Grafica_Error_Simpson = zeros(49,1);

Grafica_P(:) = vpa(P);

k = 1;

for MuestrasPorCiclo = 3:2:99
    
    X_Grafica_Convergencia(k) = MuestrasPorCiclo;


    ciclo = 0:1./(MuestrasPorCiclo.*60):1./60;
    
    P_discreta_t = subs(P_discreta_f, t, ciclo);
    P_discreta_rect = mean(P_discreta_t);
    
    Grafica_Rectangular(k) = P_discreta_rect;
    
    % calculo Simpson
    P_discreta_s = 0;
    
        for j = 1:2:MuestrasPorCiclo - 2

            P_discreta_s = P_discreta_s ...
                + P_discreta_t(j) ...
                + 4.*P_discreta_t(j+1) ...
                + P_discreta_t(j+2);

        end

    
    P_discreta_simpson = P_discreta_s./(MuestrasPorCiclo.*3);
    
    Grafica_Simpson(k) = P_discreta_simpson;
    
    P_discreta_rectangular_error = vpa(100.*abs(vpa(P_discreta_rect) ...
                                    - vpa(P))./vpa(P));
    
    Grafica_Error_Rectangular(k) = P_discreta_rectangular_error;
    
    P_discreta_simpson_error = vpa(100.*abs(vpa(P_discreta_simpson) ...
                                    - vpa(P))./vpa(P));
    
    Grafica_Error_Simpson(k) = P_discreta_simpson_error;
    
    k = k + 1;
    
    fprintf(['Numero de intervalos: %d | %%Error Rectangular: %f |' ...
        '%%Error Simpson: %f | P_rectangular: %f | P_Simpson: %f\n'], ...
        MuestrasPorCiclo, ...
        P_discreta_rectangular_error, ...
        P_discreta_simpson_error, ...
        vpa(P_discreta_rect),...
        vpa(P_discreta_simpson));

    ciclo = 0;
    P_discreta_t = 0;
    P_discreta_rect = 0;
    P_discreta_s = 0;
    P_discreta_simpson = 0;
    P_discreta_rectangular_error = 0;
    P_discreta_simpson_error = 0;

end

    subplot(2,1,1);
    plot(X_Grafica_Convergencia, Grafica_Rectangular,'-.',...
        X_Grafica_Convergencia, Grafica_Simpson,'--',...
        X_Grafica_Convergencia, Grafica_P,'-');
    xlabel('Numero de intervalos');
    ylabel('Valor de P');
    legend({'Rectangular', 'Simpson', 'Real'},'Location','southeast');
    legend('boxoff');
    grid on;
    subplot(2,1,2);
    plot(X_Grafica_Convergencia, Grafica_Error_Rectangular,'-.',...
        X_Grafica_Convergencia, Grafica_Error_Simpson,'--');
    xlabel('Numero de intervalos');
    ylabel('% Error');
    legend({'Rectangular', 'Simpson'},'Location','northeast');
    legend('boxoff');
    grid on;

clearvars
disp('>>>>>>>>>>>>>>>>>>>FIN CASO 4');
%EOF