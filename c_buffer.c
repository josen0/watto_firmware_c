/*
*  
*  c_buffer.c -   Contiene la definicion del buffer circular, ademas de las
*                 funciones para manipularlo. Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2021
*
*/

typedef struct
{
    volatile size_t in;
    volatile size_t out;
    volatile int16_t v[SIZE_C_BUFFER_TMP];
    volatile int16_t i[SIZE_C_BUFFER_TMP];
    volatile int16_t ref[SIZE_C_BUFFER_TMP];
    volatile size_t free;
}
c_buffer_tmp_t;

/*
* tmp contiene los datos del adc.
*/
c_buffer_tmp_t tmp;

/*
* Definicion del buffer circular
*/
typedef struct
{
   volatile size_t in;
   volatile size_t out;
   volatile uint8_t buff[SIZE_C_BUFFER];
   volatile size_t free;
}
c_buffer_t;

/*
* buffers circulares que manejan el flujo de datos, son declarados como
* variables globales.
*/
c_buffer_t write, read;

/*
Esta funci�n revisa si el buffer circular esta vac�o,
devuelve true si esta vacio,
false si tiene informaci�n.
*/

inline bool
isempty_c (c_buffer_t bffr)
{
   if (SIZE_C_BUFFER == bffr.free)
   {
      return true;
   }
   else
   {
      return false;
   }
};

/*
Esta funcion revisa si el buffer circular tiene alg�n dato,
devuelve true si tiene datos,
false si esta vac�o.
*/
bool
hasdata_c (c_buffer_t bffr)
{
   if (SIZE_C_BUFFER > bffr.free)
   {
      return true;
   }
   else
   {
      return false;
   }
};

/*
Esta funci�n revisa si el buffer circular esta lleno,
devuelve true si esta lleno,
false si no lo est�.
*/
inline bool
isfull_c (c_buffer_t bffr)
{
   return  0 == bffr.free;
};

/*
Funci�n que agrega un elemento al buffer circular, no devuelve valor.
*/
void
push_c (c_buffer_t *bffr, char ID)
{

   if (0 == bffr->free)
      {
         FATAL_ERROR ("c_buff lleno");
      }
      
   bffr->buff[bffr->in] = ID;
   bffr->in = ((bffr->in == (SIZE_C_BUFFER - 1)) ? 0 : bffr->in + 1);
   bffr->free--;
};

/*
Funci�n que saca un elemento del buffer circular.
Devuelve un puntero a una estructura d_buffer_t.
*/
d_buffer_t*
pop_c (c_buffer_t *bffr)
{
    char tmp_ID = 0;

    tmp_ID = bffr->buff[bffr->out];
    bffr->buff[bffr->out] = 0;
    bffr->out = ((bffr->out == (SIZE_C_BUFFER - 1)) ? 0 : bffr->out + 1);
    bffr->free++;

    switch (tmp_ID)
        {
            case 'A':
                return &A_;
                break;
            case 'B':
                return &B_;
                break;
            case 'C':
                return &C_;
                break;
            case 'D':
                return &D_;
                break;
            case 'E':
                return &E_;
                break;
            case 'F':
                return &F_;
                break;
            case 'G':
                return &G_;
                break;
            case 'H':
                return &H_;
                break;
            case 'I':
                return &I_;
                break;
            case 'J':
                return &J_;
                break;
            default:
                FATAL_ERROR("switch error");
                break;
        }
    /*
    * No deberia estar aqui.
    */
    return 0;
};

/*
Esta funci�n vac�a el buffer circular,
la cabeza y cola apuntan al mismo punto,
y el arreglo es rellenado con ceros.
*/
void
clr_c (c_buffer_t *bffr)
{
    size_t i;

    bffr->in = 0;
    bffr->out = 0;
    bffr->free = SIZE_C_BUFFER;
    for ( i = 0; i < SIZE_C_BUFFER; i++)
      {
         bffr->buff[i] = 0;
      }
      
};

void
init_c (void)
{

    size_t i;

   /*
   * vacia los buffer circulares
   */
   clr_c (&write);
   clr_c (&read);

   /* Limpia y vacia tmp*/
   tmp.in = 0;
   tmp.out = 0;
   tmp.free = SIZE_C_BUFFER_TMP;
   for (i = 0; i < SIZE_C_BUFFER_TMP; i++)
   {
       tmp.v[i] = 0;
       tmp.i[i] = 0;
       tmp.ref[i] = 0;
   }
   
   /*
   * carga los punteros de los buffers de datos al buffer circular "escritura"
   */
   push_c (&write, A_.ID);
   push_c (&write, B_.ID);
   push_c (&write, C_.ID);
   push_c (&write, D_.ID);
   push_c (&write, E_.ID);
   push_c (&write, F_.ID);
   push_c (&write, G_.ID);
   push_c (&write, H_.ID);
   push_c (&write, I_.ID);
   push_c (&write, J_.ID);
};


/*END OF FILE: c_buffer.c*/
