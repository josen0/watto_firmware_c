/*
*  
*  const.h -   Archivo cabecera que contiene constantes usadas en el programa.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
* 
*/

#ifndef CONST_H_
#define CONST_H_

#include "adj.h"

/*
* Tama�o del buffer circular global, es multiplo de SIZE_DATA_ARRAY_BUFFER
*/

#define  SIZE_C_BUFFER_TMP 650

/*
* Tama�o buffer circular.
*/
#define SIZE_C_BUFFER 10

/*
* Canales ADC a usar
* Canal de Corriente CH_I
* Canal de Voltaje   CH_V
* Canal de Referecia CH_REF
*/
enum { CH_I = 3   \
      ,CH_V = 1   \
      ,CH_REF = 0 \
      };


/*
* Constantes para el calculo del voltaje y corriente
* K_I = 125.0 / 4096.0;
* K_V = 10000.0 / 32768.0;
* K_VI = K_I * K_V
*/
//#define K_V      250.125F
//#define K_I      25.0F
//#define K_VI   6253.125F
//#define K_ADC   0.001221F
//#define K_ADC2   0.000001490841F

/*
*
* K_V_ADC = K_V * K_ADC
* K_I_ADC = K_I * K_ADC
* K_VI_ADC2 = K_V * K_I * K_ADC^2
* 
*/

#ifdef FIRMWARE

#define  K_V_ADC   0.305254F
#define  K_I_ADC   0.031266F
#define  K_VI_ADC2 0.009322415128125F

#else

#define  K_V_ADC   0.305402625F
#define  K_I_ADC   0.030525F
#define  K_VI_ADC2 0.009322415128125F

#endif

// K_KWH = 60 * 3600 * 1000
// K_WH = 60 * 3600
// K_WS = 60

#define  K_KWH 216000000.0F
#define  K_WH  216000.0F
//#define  K_WS  60.0F

/*
* C_25V es la constante que representa el valor teorico de 2.5V usando
* un convertidor de 12bits y un rango de adquisicion de 0 a 5 V
*/

#define C_25V   2047.0F

#endif /*CONST_H_*/

/*END OF FILE: const.c*/
