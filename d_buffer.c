/*
*  
*  d_buffer.c -   Contiene la definicion del buffer de datos, ademas de las
*                 funciones para manipular su acceso.
*                 Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2021
* 
*/

/*
* Definicion del buffer de datos
*/


typedef struct
{
   volatile int16_t v[SIZE_DATA_ARRAY_BUFFER];
   volatile int16_t i[SIZE_DATA_ARRAY_BUFFER];
   volatile int16_t ref[SIZE_DATA_ARRAY_BUFFER];
   volatile bool busy;
         char ID;
}
d_buffer_t;

/*
* Se definen 6 buffers para almacenar y procesar datos del ADC, son del tipo
* volatile ya que puede cambiar su valor en cualquier parte del programa.
*/
d_buffer_t A_;
d_buffer_t B_;
d_buffer_t C_;
d_buffer_t D_;
d_buffer_t E_;
d_buffer_t F_;
d_buffer_t G_;
d_buffer_t H_;
d_buffer_t I_;
d_buffer_t J_;

inline
void
init_d (void)
{
   //desbloquea los buffer de datos
   A_.busy = false;
   A_.ID = 'A';
   B_.busy = false;
   B_.ID = 'B';
   C_.busy = false;
   C_.ID = 'C';
   D_.busy = false;
   D_.ID = 'D';
   E_.busy = false;
   E_.ID = 'E';
   F_.busy = false;
   F_.ID = 'F';
   G_.busy = false;
   G_.ID = 'G';
   H_.busy = false;
   H_.ID = 'H';
   I_.busy = false;
   I_.ID = 'I';
   J_.busy = false;
   J_.ID = 'J';
};

/*END OF FILE: d_buffer.c*/
