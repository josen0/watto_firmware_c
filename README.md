# Watto_Firmware_C



## ¿Qué es este proyecto?

+ Watto es el proyecto de mi tesis. Es un medidor de energía eléctrica basado en
un microcontrolador PIC18F27K42. El código aquí mostrado fue escrito en lenguaje C y
compilado con CCS PCWH 5.091, contiene la mayoría de las rutinas necesarias para el
manejo de información, calculo de potencia promedio y valores RMS de voltaje y corriente.
El archivo principal es main.c. 

+ La carpeta test contiene un programa escrito para depurar y validar ciertos aspectos del firmware
como el cálculo de las magnitudes y el manejo de la información. Este programa puede ser
compilado con cualquier compilador de lenguaje C. El archivo principal es test.cpp

+ La carpeta validación_matlab contiene un programa para Matlab, que valida los dos algoritmos
usados para el cálculo de la energía eléctrica, teniendo como base una señal de prueba
de voltaje y corriente. Para correr este programa, la instalación de Matlab deberá tener instalado
el -Symbolic Math Toolbox-. Este programa fue ejecutado con exito en Matlab 2020b.

+ Todos los programas son funcionales.

+ Si desea ver mas sobre Watto, en el siguiente enlace puede ver el trabajo de tesis.
http://132.248.9.195/ptd2022/julio/0827653/Index.html

