/*
*  
*  tarea0.c -  Contiene la funcion tarea0,  encargada de la adquisicion
*              de datos de las se�ales Voltaje y corriente. Es invocada cada
*              vez que la interrupcion del comparador CCP1 es activada.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
*
*/

/*
* NOTA IMPORTANTE:
* El program counter (PC) es guardado en el stack dedicado PC del
* microcontrolador. Los registros del CPU son guardados incluyendo STATUS,
* WREG, BSR, FSR0/1/2, PRODL/H y PCLATH/U, tambien por el microcontrolador.
* El compilador genera codigo para salvar TABLAT y TBLPTRL/H/U.
* Si se desea mantener compatibilidad con otros modelos de microcontroladores
* debera escribir la rutina para salvar y restaurar el contexto.
*/

#ifdef   FIRMWARE
#INT_CCP1 HIGH
#endif
void
tarea0 (void)
{
   int16_t v_tmp;
   int16_t i_tmp;
   int16_t ref_tmp;
   
   int8_t tsk_tmp;
   
   /*
   * Variable para depuracion.
   * Contiene la ultima tarea ejecutada
   */
   tsk_tmp = tsk;
   tsk = 0;


#ifdef   FIRMWARE
   output_toggle(PIN_DBG);
#endif

   /*
   * Revisa si el buffer circular tmp esta lleno,
   */

   if (0 == tmp.free)
   {
      /*
      * Entonces el buffer esta lleno y es un error fatal
      */
      FATAL_ERROR("tmp lleno");
   }

   v_tmp = 0;
   i_tmp = 0;

#ifdef   FIRMWARE
   for (size_t z = 0; z < 8; z++)
   {
   
      /*
      *  Se configura el canal del ADC
      */
      set_adc_channel (CH_V);
   
      /*
      *  Se obtiene la lectura y la guarda en la estructura adc_v
      */
      v_tmp += read_adc ();

      /*
      * Configura canal para adquisicion de se�al corriente
      * Obtiene dato y lo guarda en el buffer de datos
      */
      set_adc_channel (CH_I);
      i_tmp += read_adc ();

   }
   
   v_tmp >>= 3;
   i_tmp >>= 3;

#else

   set_adc_channel (CH_V);
   v_tmp = read_adc ();
      
   set_adc_channel (CH_I);
   i_tmp = read_adc ();
      
#endif

   /*
   * Configura canal para adquisicion de se�al referencia
   * Obtiene dato y lo va acumulando.
   */
   set_adc_channel (CH_REF);
   ref_tmp = read_adc ();

   /*
   * Envia los valores de las mediciones al buffer temporal tmp
   */

   tmp.v[tmp.in] = v_tmp;
   tmp.i[tmp.in] = i_tmp;
   tmp.ref[tmp.in] = ref_tmp;
   
   /*
   * Reconfigura el indice que va marcando el inicio del buffer
   */

   tmp.in = ((tmp.in == (SIZE_C_BUFFER_TMP - 1)) ? 0 : tmp.in + 1);
   tmp.free--;


#ifdef   FIRMWARE
   output_toggle (PIN_DBG);
#endif

   tsk = tsk_tmp;

};

/*END OF FILE: tarea0.c*/
