/*
*  
*  Watto -  Firmware del proyecto de tesis Watto
*           Obtiene la energia electrica consumida.
*  Version: 2.6
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*      2022
*
*/

#include <18f27k42.h>

#device  ADC=12
#device  HIGH_INTS=TRUE
#device  ICD=TRUE

#fuses   MCLR
#fuses   HS
#fuses   RSTOSC_EXT_PLL
#fuses   NOWDT
#fuses   NOLVP
#fuses   NOPROTECT

#OCS   64 MHz

#include "local.h"

int
main (void)
{
   
   setup_oscillator (OSC_EXTOSC_ENABLED   \
                     | OSC_EXTOSC_PLL     \
                     | OSC_CLK_DIV_BY_1,  \
                        OSC_PLL_READY);

   set_analog_pins ( PIN_A0,  \
                     PIN_A1,  \
                     PIN_A2,  \
                     PIN_A3 );
   
   setup_adc_ports (sAN0 | sAN1 | sAN3, VREF_VDD);
   
   setup_adc (ADC_CLOCK_DIV_32);
   
   ADACQH = 0x00;
   ADACQL = 0x00;
   ADPREH = 0x00;
   ADPREL = 0x00;

   /*
   * Inicializando las estructuras de almacenamiento de datos
   */
   init_d ();
  
   /*
   * Inicializando las estructuras de control
   */
   init_c ();

   /*
   * Limpiando la estructura de resultado final
   */
   clr_f ();

   /*
   * inicializando el LCD
   */
   lcd_init ();
   
   /*
   * Configura el Timmer 1
   * con Fosc/4
   */
   setup_timer_1 (T1_INTERNAL | T1_DIV_BY_1);
   
   /*
   * inicializa con 0
   */
   set_timer1 (0);
   
   /*
   * Configura el Comparador 1
   */
   setup_ccp1 (CCP_COMPARE_PULSE_RESET_TIMER | CCP_USE_TIMER1_AND_TIMER2);
   
   /*
   * inicializa valor del CCP1
   */
   CCP_1 = CCP1;

   /*
   * activa interrupciones
   */
   enable_interrupts (INT_CCP1);
   enable_interrupts (GLOBAL);

   for (;;)
      {

      tarea1 ();
      tarea2 ();
      //tarea1 ();
      tarea3 ();
      //...
      //tareaN ();

      }

   return 0;
  
}

/*END OF FILE: main.c*/