/*
*  
*  tarea3.c -  Contiene la funcion tarea3, encargada de imprimir los resultados
*              de la medicion en el LCD.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
* 
*/
inline void
tarea3 (void)
{

   tsk = 3;

   /*
   * Imprime datos si hay alguna renovacion de los mismos
   */

   if (true == get_renew ())
   {

#ifdef FIRMWARE
        printf(lcd_putc, "\fV= %3.2f V\nI= %3.2f A\nP= %3.2f W\n%c= %3.2f Wh",   \
        get_rms_v(), get_rms_i(), get_p(), SIGMA, get_energia());
#else
        printf("V= %f\tV\nI= %f\tA\nP= %f\tW\n%c= %f\tWh",    \
        get_rms_v(), get_rms_i(), get_p(), SIGMA, get_energia());
#endif


      /*
      * Como los valores ya se han impreso, se pone la variable renew como
      * false.
      */
      put_renew (false);
   }
};

/*END OF FILE: tarea3.c*/
