/*
*  
*  fatal.c -   Contiene funcion de depuracion y definiciones para su
*              funcionamiento. Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*
*/

volatile int8_t tsk;
#ifdef   FIRMWARE
volatile char filename[16];
volatile char line;
volatile char msj[16];

#define  FATAL_ERROR(err)  (filename = __FILENAME__,  \
                           line = __LINE__,           \
                           msj = err,                 \
                           goto_address (0x1FF00))
/*
* Esta funcion es llamada para mostrar un error fatal y donde ocurrio.
* 0x1FF00 es una localidad valida de memoria flash de programa,
* la funcion puede ocupar de espacio el rango de 0x1FF00 a 0x1FFFF.
* 0x1FFFF es la localidad limite de la memoria de programa
* para el modelo de microcontrolador que se esta usando (18f27k42).
*/
#ORG  0x1FF00, 0x1FFFF
void
fatalerror (void)
{

   disable_interrupts (INT_CCP1);
   printf (lcd_putc, "\a\f%s:%d\n%s %i", filename, line, msj, tsk);
   //despues de mostrar el error, el microntrolador duerme
   sleep (SLEEP_FULL);

}
#else

#define  FATAL_ERROR(err)  printf("\a\f%s:%d\n%s", __FILE__, __LINE__, err)

#endif
/*END OF FILE: fatal.c*/
