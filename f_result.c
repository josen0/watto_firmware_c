/*
*  
*  f_result.c -   Contiene la definicion de estructura de datos contenedora
*                 de resultados finales y sus funciones para acceso y
*                 modificacion.
*                 Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
* 
*/

typedef struct
{
   float rms_v;
   float rms_i;
   float energia;
   float p;
   bool renew;
}
f_result_t;

f_result_t final_result;

inline void
clr_f (void)
{
   final_result.energia = 0.0;
   final_result.rms_i = 0.0;
   final_result.rms_v = 0.0;
   final_result.p = 0.0;
   final_result.renew = false;
};

void
set_rms_v (float rms_v)
{
   final_result.rms_v = rms_v;
   final_result.renew = true;
};

void
set_rms_i (float rms_i)
{
   final_result.rms_i = rms_i;
   final_result.renew = true;
};

void
set_energia (float energia)
{
   final_result.energia = energia;
   final_result.renew = true;
};

void
set_p (float p)
{
   final_result.p = p;
   final_result.renew = true;
};

inline float
get_rms_v (void)
{
   return final_result.rms_v;
};

inline float
get_rms_i (void)
{
   return final_result.rms_i;
};

inline float
get_energia (void)
{
   return final_result.energia;
};

inline float
get_p (void)
{
   return final_result.p;
};

bool
get_renew (void)
{
   return final_result.renew;
};

void
put_renew (bool renew)
{
   final_result.renew = renew;
};

/*END OF FILE: f_result.c*/
