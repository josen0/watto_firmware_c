/*
*  
*  local.h -   Archivo cabecera que contiene definiciones y otras librerias
*              usadas en el programa.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*
*/

#ifndef LOCAL_H_
#define LOCAL_H_

/*
* Compilar firmware
*/
#define FIRMWARE

/*
* Metodo de integracion del rectangulo
*/
#define RMS_RECT
#define P_RECT
//#define RMS_RECT_OVERFLOW_CHECK

/*
* Metodo de integracion por Simpson
*/
//#define RMS_SIMPSON
//#define P_SIMPSON
//#define RMS_SIMPSON_OVERFLOW_CHECK

/*
* Definiciones para el uso del LCD usando lcd.c
*/
// Necesario para 20 x 4
#define LCD_EXTENDED_NEWLINE
// Definicion de terminales para LCD
#define  LCD_RS_PIN     PIN_B1
#define  LCD_RW_PIN     PIN_B2
#define  LCD_ENABLE_PIN PIN_B3
#define  LCD_DATA4      PIN_B4
#define  LCD_DATA5      PIN_B5
#define  LCD_DATA6      PIN_B6
#define  LCD_DATA7      PIN_B7

/*
* Caracter sigma
*/
#define SIGMA 0xF6

/*
* Terminal de depuracion
*/
#define  PIN_DBG  PIN_B0

#byte ADPREH = getenv ("sfr:ADPREH")
#byte ADPREL = getenv ("sfr:ADPREL")
#byte ADACQH = getenv ("sfr:ADACQH")
#byte ADACQL = getenv ("sfr:ADACQL")

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <math.h>
#include <float.h>
#include "lcd.c"
#include "const.h"
#include "fatal.c"
#include "d_buffer.c"
#include "c_buffer.c"
#include "f_result.c"
#include "tarea0.c"
#include "tarea1.c"
#include "tarea2.c"
#include "tarea3.c"

#endif /*LOCAL_H_*/

/*END OF FILE: local.h*/
