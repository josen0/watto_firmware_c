/*
*  
*  adj.h -   Archivo cabecera que contiene constantes usadas en el programa y
               su calibracion.
*              Modifique y compile segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
* 
*/

#ifndef ADJ_H_
#define ADJ_H_


/*-*-*-*-*-*-*-*-*-*-*-*-*-*MODIFICAR CALIBRACION*-*-*-*-*-*-*-*-*-*-*-*-*-*/
/* Valor que se carga al registro CCP1 para generar la interrupcion.
*/
#define  CCP1   5925

/*
* Tama�o del buffer de datos. El numero indica cuantos datos seran adquiridos
* en voltaje y corriente.
*/
#define   SIZE_DATA_ARRAY_BUFFER    45
#define   SIZE_DATA_ARRAY_BUFFER_F  45.0F

/*-*-*-*-*-*-*-*-*-*-*-*-*-*MODIFICAR CALIBRACION*-*-*-*-*-*-*-*-*-*-*-*-*-*/


#endif /*ADJ_H_*/

/*END OF FILE: adj.h*/
