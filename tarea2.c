/*
*  
*  tarea2.c -  Contiene la funcion tarea2, encargada del calculo de la
*              energia electrica.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*   2022
* 
*/

void
tarea2 (void)
{

    d_buffer_t* reading;
    
    size_t j;
    
    int32_t v_acc;
    int32_t i_acc;
    
    int32_t v_dc;
    int32_t i_dc;

    float rms_v;
    float rms_i;
    
   float data_v_1;
   float data_i_1;

    float data_v_2, data_v_3;
    float data_i_2, data_i_3;
    
    float ECSUB;
    float Pot;
    
    static float ECA = 0.0;
    static float ECA_s = 0.0;
    static size_t count_s = 0;
    static bool rms = false;
    static size_t calc_rms= 0;
    
    tsk = 2;

    /*
    * Si esta vacio -read- no hay nada que hacer.
    */
    
   if (true == isempty_c (read))
   {
      return;
   }

   /*
   * Obtiene el puntero al buffer de datos listo para procesar
   */
   reading = pop_c (&read);

   //comprueba que no este bloqueado
   //ya que si lo esta, constituye un error en el manejo de datos
   if (reading->busy == true)
      {
        //si lo esta, es un error fatal
         FATAL_ERROR ("leer bloqu");
      }

   //bloquea el buffer para que no lo consulte ninguna otra tarea
   reading->busy = true;
   
   /*
   * Calcula la componente de DC de las se�ales tension,
   * corriente y referencia
   */

   v_acc = 0.0;
   i_acc = 0.0;
   for (j = 0; j < SIZE_DATA_ARRAY_BUFFER; j++)
   {
      v_acc += reading->v[j];
      i_acc += reading->i[j];
   }

   v_dc = v_acc / SIZE_DATA_ARRAY_BUFFER;
   i_dc = i_acc / SIZE_DATA_ARRAY_BUFFER;
   
   ECSUB = 0.0;
#ifdef P_RECT

   for (j = 0; j < SIZE_DATA_ARRAY_BUFFER; j++)
   {
   
      data_v_1 = (float) (reading->v[j] - v_dc);
      data_i_1 = (float) (reading->i[j] - i_dc);
         
      ECSUB += data_v_1 * data_i_1;
      
   }

   Pot = (K_VI_ADC2 * ECSUB) / SIZE_DATA_ARRAY_BUFFER_F;
#endif

#ifdef P_SIMPSON

   for (j = 0; j < (SIZE_DATA_ARRAY_BUFFER - 2); j+=2)
      {
         data_v_1 = (float) (reading->v[j] - v_dc);
         data_v_2 = (float) (reading->v[j+1] - v_dc);
         data_v_3 = (float) (reading->v[j+2] - v_dc);

         data_i_1 = (float) (reading->i[j] - i_dc);
         data_i_2 = (float) (reading->i[j+1] - i_dc);
         data_i_3 = (float) (reading->i[j+2] - i_dc);

         ECSUB += data_v_1 * data_i_1        \
               + 4.0F * data_v_2 * data_i_2  \
               + data_v_3 * data_i_3;
      }

   Pot = (K_VI_ADC2 * ECSUB) / (3.0F * SIZE_DATA_ARRAY_BUFFER_F);

#endif

#ifndef FIRMWARE
   set_p (Pot);
#endif
   
   ECA += Pot;

   count_s++;

   /*
   * Si la cuenta de acumulados ha llegado a 60
   * es hora de refrescar el dato de energia y potencia
   */
#ifdef   FIRMWARE
    if (60 == count_s)
    {
   
        /*
        * wattsegundo -> kilowatthora
        */
        //ECA_s += ECA / K_KWH;
        
         /*
         * wattsegundo -> watthora
         */
         ECA_s += ECA / K_WH;
   
        set_p (Pot);
        set_energia (ECA_s);

        ECA = 0.0;
        count_s = 0;
      
        if (2 == calc_rms)
        {
            rms = true;
            calc_rms = 0;
        }
        else
        {
            calc_rms++;
            rms = false;
        }
   
    }
#else

    rms = true;
   
#endif   
   /*
   * El calculo de los valores RMS se hace cada 2 segundos, ya que si el
   * calculo es continuo, el tiempo que tarda en hacerlo rebasa la velocidad
   * con la que los buffers -tmp- o -write- se vacian, obteniendo asi un error
   * de desbordamiento. Por tal motivo, se hace cada que cal_rms es 2.
   */
   
   if (true == rms) 
   {
   
      rms = false;
   
      /*
      * calcula valor RMS de la se�al voltaje y corriente, se corrigen los datos
      * restando el valor DC que se obtuvo anteriormente, ademas de realizar la
      * multiplicacion punto a punto de voltaje y corriente
      */
      
      rms_v = 0.0;
      rms_i = 0.0;
#ifdef RMS_RECT

      for (j = 0; j < SIZE_DATA_ARRAY_BUFFER; j++)
      {
      
         rms_v += pow((float) (reading->v[j] - v_dc), 2);
         rms_i += pow((float) (reading->i[j] - i_dc), 2);
      
   #ifdef RMS_RECT_OVERFLOW_CHECK
         /*
         * Revision de overflow.
         * valores RMS siempre positivos.
         */
         if (FLT_MAX <= rms_v || FLT_MAX <= rms_i)
         {
            /*
            * Si hay una falla es un error fatal y por el momento no hay forma
            * de tratarlo.
            */
            FATAL_ERROR("overflow rms_v/i");
         }
         
         if (0.0F > rms_v || 0.0F > rms_i)
         {
            /*
            * Si hay una falla es un error fatal y por el momento no hay forma
            * de tratarlo.
            */
            FATAL_ERROR("corrupcion rms_v/i");
         }
         
   #endif
         
      }
         
      //printf(lcd_putc,"\frms %f", rms_v);
      rms_v /= SIZE_DATA_ARRAY_BUFFER;
      rms_i /= SIZE_DATA_ARRAY_BUFFER;
      //printf(lcd_putc,"\n div %f", rms_v);

#endif

#ifdef RMS_SIMPSON

      for (j = 0; j < (SIZE_DATA_ARRAY_BUFFER - 2); j+=2)
      {
      
         data_v_1 = (float) (reading->v[j] - v_dc);
         data_v_2 = (float) (reading->v[j+1] - v_dc);
         data_v_3 = (float) (reading->v[j+2] - v_dc);

         data_i_1 = (float) (reading->i[j] - i_dc);
         data_i_2 = (float) (reading->i[j+1] - i_dc);
         data_i_3 = (float) (reading->i[j+2] - i_dc);

         /*
         * funcion pow en otros compiladores regresa un double.
         * En CCS los double no estan definidos. Si se compila este programa
         * con un compilador diferente, donde pow regrese un double, tendra
         * un aviso de warning sobre perdida de informacion. Para corregirlo
         * debera definir las variables y los cast necesarios como double. 
         */
         
         rms_v += pow (data_v_1, 2)       \
               + 4.0F * pow (data_v_2, 2) \
               + pow (data_v_3, 2);

         rms_i += pow (data_i_1, 2)       \
               + 4.0F * pow (data_i_2, 2) \
               + pow (data_i_3, 2);
               
   #ifdef RMS_SIMPSON_OVERFLOW_CHECK
         /*
         * Revision de overflow.
         * valores RMS siempre positivos.
         */
         if (FLT_MAX <= rms_v || FLT_MAX <= rms_i)
         {
            /*
            * Si hay una falla es un error fatal y por el momento no hay forma
            * de tratarlo.
            */
            FATAL_ERROR("overflw rms_v/i");
         }
         
         if (0.0F > rms_v || 0.0F > rms_i)
         {
            /*
            * Si hay una falla es un error fatal y por el momento no hay forma
            * de tratarlo.
            */
            FATAL_ERROR("corrupcion rms_v/i");
         }
   #endif

      }
      
      //printf(lcd_putc,"\frms %f", rms_v);
      rms_v /= ( 3.0F * SIZE_DATA_ARRAY_BUFFER_F);
      rms_i /= ( 3.0F * SIZE_DATA_ARRAY_BUFFER_F);
      //printf(lcd_putc,"\n div %f", rms_v);

#endif

      rms_v = sqrt(rms_v);
      rms_i = sqrt(rms_i);

      /*
      * Env�a datos para impresion
      */
      set_rms_v (K_V_ADC * rms_v);
      set_rms_i (K_I_ADC * rms_i);
      
   }
   
   /*
   * Desbloquea el buffer de datos, al cual apunta -leer-
   */
   reading->busy = false;

   /*
   * revisa si el buffer escritura esta lleno, si lo esta es un error fatal
   * ya que no hay donde poner el buffer liberado
   */   
   if (isfull_c (write))
      {
         FATAL_ERROR(">c buff lleno!<");
      }

   push_c (&write, reading->ID);

};

/*END OF FILE: tarea2.c*/
