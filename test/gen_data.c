/*
*
*  gen_data.c -  Contiene las funciones para generar dos vectores de datos
*				 para la simulacion del ADC.
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*	2022
*
*/

double
degtorad(double degree)
{
	return degree * (M_PI / 180.0);
};

double
v_caso4 (double t)
{
	double v1, v3, v5, v7;
	v1 = sqrt(2) * 100 * sin(2 * M_PI * 60 * t);
	v3 = sqrt(2) * 20 * sin(3 * 2 * M_PI * 60 * t - degtorad(70));
	v5 = sqrt(2) * 25 * sin(5 * 2 * M_PI * 60 * t - degtorad(-140));
	v7 = sqrt(2) * 10 * sin(7 * 2 * M_PI * 60 * t - degtorad(-210));
	return (v1 + v3 + v5 + v7);
};

double
i_caso4 (double t)
{
	double i1, i3, i5, i7;

	i1 = sqrt(2) * 60 * sin(2 * M_PI * 60 * t - degtorad(30));
	i3 = sqrt(2) * 15 * sin(3 * 2 * M_PI * 60 * t - degtorad(165));
	i5 = sqrt(2) * 12 * sin(5 * 2 * M_PI * 60 * t - degtorad(-285));
	i7 = sqrt(2) * 10 * sin(7 * 2 * M_PI * 60 * t - degtorad(-310));

	return (i1 + i3 + i5 + i7);
};



int32_t v_caso4_data[SIZE_DATA_ARRAY_BUFFER];
int32_t i_caso4_data[SIZE_DATA_ARRAY_BUFFER];

void
gendata (void)
{
	double t[SIZE_DATA_ARRAY_BUFFER];

	double k = 0.0;

	// Genera vector de tiempo t
	printf("Generacion de vector tiempo t\n");
	for (size_t j = 0; j < SIZE_DATA_ARRAY_BUFFER; j++)
	{
		t[j] = k;

		k += 1 / (60.0 * SIZE_DATA_ARRAY_BUFFER_F);

		printf("[%i] t - [%lf]\n",j ,t[j]);
	}

	// evalua las funciones v i con los datos del vector tiempo t
	printf("Generacion de vector de datos\n");
	for (size_t j = 0; j < SIZE_DATA_ARRAY_BUFFER; j++)
	{

		v_caso4_data[j] = (int32_t) ( v_caso4 ( t[j] ) / (K_V_ADC) + C_25V);
		i_caso4_data[j] = (int32_t) ( i_caso4 ( t[j] ) / (K_I_ADC) + C_25V);

		printf("V - [%i]\tI - [%i]\n", v_caso4_data[j], i_caso4_data[j]);

	}

};

/*END OF FILE: gen_data.c*/