/*
*
*  simul_adc.c -  Contiene las funciones que simulan el ADC.
*					necesarias para el programa prueba test.cpp
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*	2021
* 
*/


uint8_t channel;

int32_t
read_adc (void)
{
	static size_t v_index = 0, i_index = 0;
	int32_t res;
	
	switch (channel)
		{
			case CH_V:
				if (SIZE_DATA_ARRAY_BUFFER == v_index)
				{
					v_index = 0;
				}
				res = v_caso4_data[v_index];
				v_index++;
				break;
			
			case CH_I:
				if (SIZE_DATA_ARRAY_BUFFER == i_index)
				{
					i_index = 0;
				}
				res = i_caso4_data[i_index];
				i_index++;
				break;

			case CH_REF:
				res = 0x07FF;
				break;

			default:
				FATAL_ERROR ("Numero no valido en switch");
				break;

		}
	
	
	return res;
};

void
set_adc_channel (uint8_t ch)
{
	channel = ch;
};

/*END OF FILE: simul_adc.c*/