/*
*
*	test.c - Programa prueba del firmware watto
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   kkeezzff@hotmail.com
*	2022
*
*/

#define _USE_MATH_DEFINES

/*
#define RMS_RECT
#define P_RECT
#define RMS_RECT_OVERFLOW_CHECK
*/

#define RMS_SIMPSON
#define P_SIMPSON
#define RMS_SIMPSON_OVERFLOW_CHECK

#define SIGMA 0x53

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <math.h>
#include "float.h"
#include "..\const.h"
#include "..\fatal.c"
#include "gen_data.c"
#include "simul_adc.c"
#include "..\d_buffer.c"
#include "..\c_buffer.c"
#include "..\f_result.c"
#include "..\tarea0.c"
#include "..\tarea1.c"
#include "..\tarea2.c"
#include "..\tarea3.c"
#include "show_data.c"

int
main (void)
{
	size_t i;

	// genera vectores de datos para la simulacion del ADC
	gendata();

	// inicializa las estructuras de datos a usar.
	init_d ();
	init_c ();
	clr_f ();
	printf ("1 - estructuras de datos inicializadas\n");
	show_c ();
	show_d ();
	show_tmp ();
	printf ("2 - Se inicia la simulacion de %i interrupciones\n",\
		SIZE_DATA_ARRAY_BUFFER + 26);
	for (i = 0; i < SIZE_DATA_ARRAY_BUFFER + 26 ; i++)
		{
			tarea0 ();
		}
	printf("3 - Se muestran las estructuras de datos despues de llamada a "\
														"interrupcion\n");
	show_c ();
	show_d ();
	show_tmp ();

	printf("4 - Se llama a la funcion de transferencia de datos entre "\
															"buffers\n");
	tarea1 ();
	show_d ();
	show_c ();
	show_tmp ();

	printf("5 - Se llama a la funcion de calculo de la energia\n");
	tarea2 ();
	show_d ();
	show_c ();
	show_tmp ();

	printf("6 - Se imprimen datos finales\n");
	tarea3 ();
	
	return 0;

}

/*END OF FILE: test.c*/