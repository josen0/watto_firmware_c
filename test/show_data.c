/*
*
*  show_data.c -  Contiene las funciones para mostrar datos y estatus de las
*					estructuras de datos y de control,  necesarias para el
*					programa prueba test.cpp
*              Modifique segun sea necesario.
*  Autor:   J. Nieto
*  email:   jnietojuarez@gmail.com
*	2021
* 
*/

void
show_c (void)
{
	size_t i;
	printf("\n[write]\nfree:%i\n{", write.free);
	for (i = 0; i < SIZE_C_BUFFER; i++)
		{

		if (i == write.in)
			{
				printf ("(in)->");
			}
		if (i == write.out)
			{
				printf ("(out)->");
			}

			printf ("[%c]\t", write.buff[i]);
		}
	printf ("}\n");

	printf ("[read]\nfree:%i\n{", read.free);
	for (i = 0; i < SIZE_C_BUFFER; i++)
		{
			if (i == read.in)
				{
					printf("(in)->");
				}
			if (i == read.out)
				{
					printf("(out)->");
				}

			printf ("[%c]\t", read.buff[i]);
		}
	printf("}\n");

};

void
show_d(void)
{
	size_t i;
	printf ("\n[%c]\t\t[%c]\t\t[%c]\t\t[%c]\t\t[%c]\t\t[%c]\n{",\
			A_.ID, B_.ID, C_.ID, D_.ID, E_.ID, F_.ID);

	for (i = 0; i < SIZE_DATA_ARRAY_BUFFER; i++)
	{
		printf	("V-[%X] I-[%X] REF-[%X] BUSY-[%X]\t\
				V-[%X] I-[%X] REF-[%X] BUSY-[%X]\t\
				V-[%X] I-[%X] REF-[%X] BUSY-[%X]\t\
				V-[%X] I-[%X] REF-[%X] BUSY-[%X]\t\
				V-[%X] I-[%X] REF-[%X] BUSY-[%X]\t\
				V-[%X] I-[%X] REF-[%X] BUSY-[%X]\n",\
				A_.v[i], A_.i[i], A_.ref[i], A_.busy,\
				B_.v[i], B_.i[i], B_.ref[i], B_.busy,\
				C_.v[i], C_.i[i], C_.ref[i], C_.busy,\
				D_.v[i], D_.i[i], D_.ref[i], D_.busy,\
				E_.v[i], E_.i[i], E_.ref[i], E_.busy,\
				F_.v[i], F_.i[i], F_.ref[i], F_.busy\
				);
	}

	printf("}\n");

};

void
show_tmp(void)
{
	size_t i;
	printf("TMP {\n");

	for (i = 0; i < SIZE_C_BUFFER_TMP; i++)
	{
		printf("V-[%X]\tI-[%X]\tREF-[%X]\t\n", tmp.v[i], tmp.i[i], tmp.ref[i]);
	}

	printf("}\n");

};

/*END OF FILE: show_data.c*/